'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const listSchema = Schema({
    urlImage: String,
    description: String
})

module.exports = mongoose.model('ListItem', listSchema)