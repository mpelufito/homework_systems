const gulp = require('gulp')
const babel = require('gulp-babel')
const sass = require('gulp-sass')
var htmlmin = require('gulp-htmlmin');

gulp.task('html', function() {
  return gulp.src('app/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'));
});
gulp.task('uploadFolder', function() {
  return gulp.src('app/upload')
    .pipe(gulp.dest('dist/'));
});
gulp.task('js', () =>
  gulp.src('./app/js/app.js')
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(gulp.dest('./dist/js'))
);

gulp.task('sass', function () {
  return gulp.src('./app/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./app/sass/**/*.scss', ['sass']);
});
gulp.task('html:watch', function () {
  gulp.watch('./app/*.html', ['html']);
});
gulp.task('js:watch', function () {
  gulp.watch('./app/js/**/*.js', ['js']);
});

gulp.task('serve', [ 'html:watch','sass:watch', 'js:watch', 'uploadFolder' ]);
gulp.task('dist', [ 'html','sass', 'js','uploadFolder' ]);
