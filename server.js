const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const path = require('path')
const ListItem = require('./models/info')
var formidable = require('formidable');
var fs = require('fs');

const server = express()
server.use(express.static('dist/'));

server.use(bodyParser.urlencoded({ extended: false }))
server.use(bodyParser.json())

/* server.get('/upload', function (req, res) {
    res.sendFile(__dirname+'/upload.html')
}) */
server.post('/upload', function (req, res) {
  var urlImage
  var form = new formidable.IncomingForm()
  form.multiples = true
  form.uploadDir = path.join(__dirname, '/dist/upload')
  form.on('file', function (field, file) {
    urlImage = file.name
    fs.rename(file.path, path.join(form.uploadDir, file.name))
  })
  form.on('error', function (err) {
  })
  form.on('end', function () {
    res.end(`./upload/${urlImage}`)
  });

  form.parse(req)
});

server.post('/newItem', (req, res) => {
  let item = new ListItem()
  item.urlImage = req.headers.dataurl
  item.description = req.headers.datatext
  id = req.headers.dataid

  const dataUpdate = {
    urlImage: item.urlImage,
    description: item.description
  }
  if (id) {
    ListItem.findByIdAndUpdate(id, dataUpdate, (err, itemUpdate) => {
      if (err) res.status(500).end('fallo la carga en la base de datos al actualizar')
      res.status(200).send('Se actualizo correctamente')
    })
  } else {
    item.save((err, itemStored) => {
      if (err) res.status(500).end('fallo la carga en la base de datos')

      res.status(200).send('carga correcta de los datos')
    })
  }


})

server.get('/listItem', (req, res) => {
  ListItem.find({}, (err, items) => {
    if (err) res.status(500).end('fallo la carga en la base de datos')
    res.status(200).send({ items })
  })
})

server.post('/deleteItem', function (req, res) {
  const idItem = req.headers.itemselected
  ListItem.findById(idItem, function (err, item) {
    if (err) res.status(500).end('fallo la carga en la base de datos al borrar')
    item.remove(function (err) {
      if (err) res.status(500).end('fallo la carga en la base de datos al borrar')
      res.status(200).send(true)

    })
  })
})


mongoose.connect('mongodb://localhost:27017/list', (err, res) => {
  if (err) throw err
  console.log('Connected successfully to DB')
  server.listen(3030, function () {
    console.log(' Connected successfully to system')
  })
})


