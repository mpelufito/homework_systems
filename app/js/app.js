'use strict';
$(function () {
  loadData()
  $('.modal').modal({
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '4%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
    complete: function () {
      $('#UpdateData').remove()
      $('#textarea').val("")
      $('#urlFile').val("")
      $('#alertStatus').text('')
      $('#idItem').val("")
      $("#loadData").css({
        'display': 'inline-block'
      });
      $("#updateData").css({
        'display': 'none'
      });
    }

  });

  $("#listItem").sortable({});
  $("#listItem").disableSelection();

  $('#loadData').on('click', function () {
    const dataFile = $('#upload-input').get(0).files;
    let dataText = $('#textarea').val()

    loadImage(dataFile, dataText)
  });

  $('#updateData').on('click', function () {
    let idItemUpdate = $('#idItem').val()
    let updateDescription = $('#textarea').val()
    let updateImage = $('#urlFile').val()
    const updateData = {
      'dataid': idItemUpdate,
      'dataText': updateDescription,
      'dataUrl': updateImage
    }
    const dataFileUpdate = $('#upload-input').get(0).files;
    loadImage(dataFileUpdate, updateData)

  })
});



function loadImage(files, dataText) {
  const allowedList = ['image/png', 'image/jpeg', 'image/gif']

  if (allowedList.indexOf(files[0].type) < 0) {
    $('#alertStatus').text('Only images are allowed')
  } else {
    if (files.length > 0) {
      let formData = new FormData();
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        formData.append('uploads[]', file, file.name);
      }
      $.ajax({
        url: '/upload',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
          const newData = {
            'dataText': dataText,
            'dataUrl': data
          }
          if (typeof (dataText) === "string") {
            submitData(newData)
          } else {
            dataText.dataUrl = data
            submitData(dataText)

          }
        },
        err: function (err) {
          $('#alertStatus').text('upload error!', err)
        }
      });
    } else {
      $('#alertStatus').text('Empty image!')
    }
  }
}

function submitData(data) {
  $.ajax({
    url: '/newItem',
    type: 'POST',
    headers: data,
    processData: false,
    contentType: false,
    success: function (data) {
      $('#alertStatus').text('upload successful!')

      setTimeout(() => {
        $('#modalNewItem').modal('close');
        loadData()
      }, 1500)
    },
    err: function (err) {
      $('#alertStatus').text('upload error!', err)
    }
  });
}
function loadData() {
  let containerList = $("#listItem")
  containerList.empty()
  $.ajax({
    url: '/listItem',
    type: 'get',
    success: function (data) {
      const newData = data.items
      for (var key in newData) {
        var element = newData[key]
        containerList.append(
          `<li>
          <div id="${element._id}">
            <p> ${element.description} </p>
            <img class="imgViewer" src="${element.urlImage}" alt="${element.urlImage}" />
            <div class="containerBtn">
              <button id="clear${element._id}" class=" right deleteItem btn-floating red lighten-2 waves-effect waves-light"><i class=" material-icons ion-trash-b"></i></button>
              <button id="edit${element._id}" class=" right editItem btn-floating green lighten-2 waves-effect waves-light"><i class=" material-icons ion-edit"></i></button>
            </div>
            </div>
        </li>`)
      }
      deleteItems()
      editItems()
    },
    err: function (err) {
    }
  });
}

function deleteItems() {
  $('.deleteItem').on('click', function () {
    let itemselected = $(this).attr('id').substring(5)

    $.ajax({
      url: '/deleteItem',
      type: 'post',
      headers: { itemselected },
      success: function (data) {
        loadData()
      },
      err: function (err) {
      }
    });
  })
}
function editItems() {
  $('.editItem').on('click', function () {
    $('.modal').modal('open')
    let itemselected = $(this).attr('id').substring(4)
    let dataDesciption = $('#' + itemselected).children('p').text()
    let dataImagen = $('#' + itemselected).children('img').attr('src')
    $('#idItem').val(itemselected)
    $('#textarea').val(dataDesciption)
    $('#urlFile').val(dataImagen)

    $("#loadData").css({
      'display': 'none'
    });
    $("#updateData").css({
      'display': 'inline-block'
    });
  })
}

